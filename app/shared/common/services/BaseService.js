
rpApp.factory("baseService",
["$http",
function ($http) {

    var baseService = {};
    baseService.getData = function (callBack, controlObj, dataObj) {
        var baseUrl = 'app/staticData/';
        var req = {
            method: 'GET',
            url: baseUrl + controlObj.callPath,
            data: dataObj
        }

        $http(req).then(function (response) {

            //alert("response ::::"+response.data)
            callBack(response.data);
        }, function (response) {
            alert("error :::" + response.status);
        });
    }
    return baseService;

} ]);




