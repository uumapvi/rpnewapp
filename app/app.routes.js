/* 
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

rpApp.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/search');
    $stateProvider
            .state('Search', {
                url: '/search',
                templateUrl: 'app/components/FSOD/Search/TemplateSearch.htm',
                controller: 'ContSearch'
            })
            .state('StudentResult', {
                url: '/searchResult/:FName/:LName/:DOB/:TestDate',
                templateUrl: 'app/components/FSOD/SearchStudentResult/TemplateSearchStudentResult.htm',
                controller: 'ContSearchStudentResult'
            })
            .state('CohortResult', {
                url: '/searchResult/:Month',
                templateUrl: 'app/components/FSOD/SearchCohortResult/TemplateSearchCohortResult.htm',
                controller: 'ContSearchCohortResult'
            })
            .state('IndividualStudent', {
                url: '/individualStudent/:LearnerId',
                templateUrl: 'app/components/FSOD/IndividualStudent/TemplateIndividualStudent.htm',
                controller: 'ContIndividualStudent'
            })
            .state('IndividualStudentResult', {
                url: '/individualStudentResult/:LearnerId/:PRCode/:UnitCode',
                templateUrl: 'app/components/FSOD/IndividualStudentResult/TemplateIndividualStudentResult.htm',
                controller: 'ContIndividualStudentResult'
            })
});



