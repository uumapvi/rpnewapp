/* 
* Copyright 2016 Pearson Education. All rights reserved.
* 
*/


rpApp.controller("ContIndividualStudent",
["$scope", "$log", "$location", "$stateParams", "baseService",
function ($scope, $log, $location, $stateParams, $baseService) {

    $scope.LearnerId = $stateParams.LearnerId;
    $scope.qualificationResults = [];
    $scope.onEnd = function () {
        jq('.accordian-row').addClass("accordianTrHide");
        jq('.drilldown').click(function () {
            jq(this).find("i").toggleClass("fa-angle-down").toggleClass("fa-angle-up")
            jq(this).parent().parent("tr").toggleClass("accordian-row-opened");
            jq(this).parent().parent("tr").next(".accordian-row").toggleClass("accordianTrHide").toggleClass("accordianTrShow")
        });
    };

    var onDataLoaded = function (data) {
        //alert("data:::"+data);
        //$scope.qualifications = data.Envelope.Body.GetLearnerAchievementResponse.GetLearnerAchievementResult.QualificationTypes.QualificationType;
        angular.forEach(data.Envelope.Body.GetLearnerAchievementResponse.GetLearnerAchievementResult.QualificationTypes.QualificationType, function (value, key) {
            angular.forEach(value.QualificationResults.QualificationResult, function (result) {
                result.QualificationTypeCode = value.QualificationTypeCode;
                $scope.qualificationResults.push(result);
            })


        });
    }

    var dataObj = {};

    if (typeof (LearnerId) !== "undefined") {
        dataObj.LearnerId = LearnerId;
    }

    var controlObj = {};
    controlObj.callPath = "GetSubjectsByStudent.json";
    $baseService.getData(onDataLoaded, controlObj, dataObj);

} ]).
        directive("repeatEnd", function () {
            return {
                restrict: "A",
                link: function (scope, element, attrs) {
                    if (scope.$last) {
                        scope.$eval(attrs.repeatEnd);
                    }
                }
            };
        });
        