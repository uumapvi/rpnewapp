/* 
* Copyright 2016 Pearson Education. All rights reserved.
* 
*/
rpApp.controller("ContSearchStudentResult",
["$scope", "$log", "$location", "$stateParams", "baseService",
function ($scope, $log, $location, $stateParams, $baseService) {

    $log.log("searchResultCtr initialized...");
    $scope.FName = $stateParams.FName;
    $scope.LName = $stateParams.LName;
    $scope.DOB = $stateParams.DOB;
    $scope.TestDate = $stateParams.TestDate;

    var onDataLoaded = function (data) {

        $scope.learners = data.Envelope.Body.FindLearnerByCentreResponse.FindLearnerByCentreResult.Learners.Learner;
    }

    var dataObj = {};

    if (typeof (fName) !== "undefined") {
        dataObj.fName = fName;
    }
    if (typeof (lName) !== "undefined") {
        dataObj.lName = lName;
    }
    if (typeof (dob) !== "undefined") {
        dataObj.dob = dob;
    }
    if (typeof (testDate) !== "undefined") {
        dataObj.testDate = testDate;
    }
    var controlObj = {};
    controlObj.callPath = "FindLearnerByCentre.json";
    $baseService.getData(onDataLoaded, controlObj, dataObj);
} ]);

rpApp.filter('formatDate', function () {
    return function (x) {
        var myDate;
        myDate = x.split("T")[0];

        return myDate.toString();
    };
});